module Chapter02.SimpleFunctions
    ( (+++)
    , fibonacci
    , firstOrNothing
    , minmax
    , reverse2
    )
where

(+++) :: [a] -> [a] -> [a]
[]       +++ lst2 = lst2
(x : xs) +++ lst2 = x : (xs +++ lst2)

fibonacci :: Integer -> Maybe Integer
fibonacci n
    | n < 0
    = Nothing
    | n == 0
    = Just 0
    | n == 1
    = Just 1
    | otherwise
    = let Just f1 = fibonacci (n - 1)
          Just f2 = fibonacci (n - 2)
      in  Just (f2 + f1)

firstOrNothing :: [String] -> Maybe String
firstOrNothing []      = Nothing
firstOrNothing (x : _) = Just x

minmax :: Ord a => [a] -> (Maybe a, Maybe a)
minmax []       = (Nothing, Nothing)
minmax (x : []) = (Just x, Just x)
minmax (x : xs) =
    ( if x < xs_min then Just x else Just xs_min
    , if x > xs_max then Just x else Just xs_max
    )
    where (Just xs_min, Just xs_max) = minmax xs

reverse2 :: [a] -> [a]
reverse2 []       = []
reverse2 (x : xs) = (reverse2 xs) +++ (x : [])
