module Chapter02.DataTypes where

data Client
  = GovOrg { clientName :: String }
  | Company { clientName :: String
            , companyId  :: Integer
            , person     :: Person
            , duty       :: String }
  | Individual { person  :: Person
               , showAds :: Bool }
  deriving (Show)

data Gender
  = Male
  | Female
  | Unknown
  deriving (Show)

data TimePeriod
  = Past
  | Future
  | Both
  deriving (Show)

data Person = Person
  { firstName :: String
  , lastName  :: String
  , gender    :: Gender
  } deriving (Show)

data TimeMachine = TimeMachine
  { manufacturer :: String
  , model        :: Integer
  , name         :: String
  , timePeriod   :: TimePeriod
  , price        :: Double
  } deriving (Show)
