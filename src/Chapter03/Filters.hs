module Chapter03.Filters
  ( filterOnes
  )
where

filterOnes :: [Integer] -> [Integer]
filterOnes = filter (\x -> x == 1)
